package com.loanit.loanit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity {

    private static final String TAG = "login";
    Button but;
    EditText text1, text2;
    private FirebaseAuth mAuth;


    public void btnletreg() {


        Intent r = new Intent();
        r.setClass(login.this, registration.class);
        startActivity(r);

    }


    public void login() {


        mAuth = FirebaseAuth.getInstance();

        EditText text = findViewById(R.id.editText);
        final String email = text.getEditableText().toString();

        EditText textpassword = findViewById(R.id.editText2);
        final String password = textpassword.getEditableText().toString();


        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(login.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Intent i = new Intent();
                            i.setClass(login.this, homepage.class);
                            startActivity(i);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(login.this, "Authentication failed. ",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        public void onStart {
//            super.onStart();
//            // Check if user is signed in (non-null) and update UI accordingly.
//            FirebaseUser currentUser = mAuth.getCurrentUser();
//            updateUI(currentUser);
//        }
// ...


        Button login = findViewById(R.id.button4);
// Register the onClick listener with the implementation above
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                login();
                //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
            }
        });


        TextView letreg = findViewById(R.id.textView8);
// Register the onClick listener with the implementation above
        letreg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                btnletreg();
                //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
            }


        });
    }

//        EditText et = (EditText) findViewById(R.id.editText);
//        String text= et.getEditableText().toString();


};



